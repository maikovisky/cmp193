# Cria tabel com timestamp as ts
db.measures.aggregate([
    {"$project":{"ts": {""$toLong":"$timestamp"},"timestamp": 1,"hrProcessorLoad" : 1,"interface" : 1,"ifInOctets" : 1,"ifOutOctets" : 1,"ifInErrors" : 1,"ifOutErrors" : 1,"ifInUcastPkts" : 1,"ifOutUcastPkts" : 1,"ifOutDiscards" : 1}},{$out: "measures_ts"} ])


db.measures.aggregate([
    { $group: {
        "_id": "$interface",
        "name": {$first: "$interface"},
        "ifInOctets" :{$avg: "$ifInOctets"},
        "ifOutOctets" :{$avg: "$ifOutOctets"}
    }},
    {
        $group: {"_id": 0, array: {$push: {"name": "$name", "ifInOctets": "$ifInOctets", "ifOutOctets": "$ifOutOctets"}}}
    },
    { $unwind: { path: "$array", includeArrayIndex: "arrayIndex" }}, 
    {$project: {_id: { $add: ["$arrayIndex", 1]}, name: "$array.name", ifInOctets: "$array.ifInOctets", ifOutOctets: "$array.ifOutOctets" }},
    {$out: "interfaces"}
])