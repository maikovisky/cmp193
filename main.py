#
# Trabalho disciplina de Gerencia de Redes
#
# Autores: Huina Grabriela Pereira e Maiko de Andrade
#

import requests
import pandas as pd
import numpy as np
import json
import glob
from pymongo import MongoClient
import matplotlib
import matplotlib.pyplot as plt
from slugify import slugify


# Pega todos os arquivos 
def main():
    files = glob.glob("measures\\host_1_measure_value_*.json")
    #files = glob.glob("measures\\*.json")
    
    for f in files:
        print("File: " + f)
        mergeFile(f) 
        #exit()


def getHost():
    print("Get hosts", flush=True)
    r = requests.get("http://143.54.2.90/api/host")

    hosts = pd.read_json(r.text)
    csv = hosts.to_csv(r'measures\hosts.csv', index = None, header = True)

    for h in hosts['id']:
        getMeasure(h)

def mergeFile(file):

    measures = pd.read_json(file)
    measures = measures.dropna()

    #df.append({"timestamp": measures['timestamp']})
    #df = pd.
    cols = measures.columns
    cols = cols.drop(['timestamp', 'hrProcessorLoad']).tolist()
    #print(cols)
    dfRet = pd.DataFrame()
    for row in measures.iterrows():
        #print(row[1].timestamp)
        #print(row[1].hrProcessorLoad)
        df = row[1]

        a = np.array([]).tolist()
        a.append(df.timestamp)
        a.append(df.hrProcessorLoad)
        #dtypes =  ['datetime', 'int', 'str', 'int', 'int', 'int', 'int', 'int', 'int', 'int']
        for r in cols:
            #print(df[r].count())
            b = pd.DataFrame.from_dict(df[r], orient="index").to_numpy().flatten().tolist()
            c = a + [r] + b          
            dfAux = pd.DataFrame([c], columns=['timestamp', 'hrProcessorLoad', 'interface', 'ifInOctets', 'ifOutOctets', 'ifInErrors', 'ifOutErrors', 'ifInUcastPkts', 'ifOutUcastPkts', 'ifOutDiscards'])
            dfRet = dfRet.append(dfAux, ignore_index=True)

    #dfRet.plot.line(x='timestamp',y='hrProcessorLoad')
    dfRet['timestamp'] = pd.to_datetime(dfRet['timestamp'])
    dfRet = dfRet.astype({'ifInOctets': 'int64', 'ifOutOctets': 'int64'})
    dfRet['ifInOctets'] = pd.to_numeric(dfRet['ifInOctets'])
    dfRet['ifOutOctets'] = pd.to_numeric(dfRet['ifOutOctets'])
    dfRet['ifInErrors'] = pd.to_numeric(dfRet['ifInErrors'])
    dfRet['ifOutErrors'] = pd.to_numeric(dfRet['ifOutErrors'])
    dfRet['ifInUcastPkts'] = pd.to_numeric(dfRet['ifInUcastPkts'])
    dfRet['ifOutUcastPkts'] = pd.to_numeric(dfRet['ifOutUcastPkts'])
    dfRet['ifOutDiscards'] = pd.to_numeric(dfRet['ifOutDiscards'])
    #lines.show()
    #print(dfRet)
    #print(dfRet.dtypes)
    #genGraph(dfRet)
    saveCollection(dfRet)
    #exit()

def saveCollection(df):
    client = MongoClient()
    db = client["cmp193"]
    collection = db.measures
    records = df.to_dict(orient='records')
    collection.insert_many(records)


def genGraph(dfMeasures):

    dfInterfaces = dfMeasures.groupby(['interface']).size().reset_index(name='counts')   
    #print(dfInterfaces)
    
    for row in dfInterfaces.iterrows():
        interface = row[1].interface
        #print(interface)
        df = dfMeasures.query('interface.str.startswith(@interface)')
        #print(df['ifInOctets'].mean())
        if(df.empty or df['ifInOctets'].mean() == 0.0):
            print("Vazio: " + interface)
            continue
        print("Graph: " + interface)
        print(df.describe(include='all',  percentiles=[.25, .5, .75]))

        #fig  = plt.figure(figsize = (25,8))
        fig, (ax11, ax12, ax13, ax14) = plt.subplots(4, 1, figsize = (24,16))
        #ax1.set_title("Serie da Interface " + interface) 
        #ax1.grid(True)
        df.set_index('timestamp', inplace=True)
        df['ifInOctets'].plot(legend=True, ax=ax11)
        df['ifOutOctets'].plot(legend=True, ax=ax11)
        df['ifInErrors'].plot(legend=True, ax=ax12)
        df['ifOutErrors'].plot(legend=True, ax=ax12)
        df['ifInUcastPkts'].plot(legend=True, ax=ax13)
        df['ifOutUcastPkts'].plot(legend=True, ax=ax13)
        df['ifOutDiscards'].plot(legend=True, ax=ax14)
        df['hrProcessorLoad'].plot(secondary_y=True, legend=True)
        fig.suptitle("Serie da Interface " + interface, fontsize=24 )
        fig.savefig("measures\\interface-"+ slugify(interface) +"-plot.png")
        plt.close(fig)
        
        
        fig2, ax2 = plt.subplots(figsize = (15,8))
        ax2.set_title("Boxplot da Interface " + interface) 
        ax2 = df.boxplot()
        fig2.savefig("measures\\interface-"+ slugify(interface) +"-boxplot.png")
        plt.close(fig2)
        

        fig3, ax3 = plt.subplots(figsize = (15,8))
        fig3.canvas.set_window_title("Hitograms interface " + interface)
        df.hist(ax=ax3, column=['hrProcessorLoad', 'ifInOctets', 'ifOutOctets', 'ifInErrors', 'ifOutErrors', 'ifInUcastPkts', 'ifOutUcastPkts', 'ifOutDiscards'])
        fig3.suptitle("Hitograms interface " + interface)
        fig3.savefig("measures\\interface-"+ slugify(interface) +"-histogram.png")
        plt.close(fig3)

        plt.close('all')
        #break


def getMeasure(hostId):
    print("Get measures from host " + str(hostId), flush=True)
    r = requests.get("http://143.54.2.90/api/host/" + str(hostId) + "/measure/" )
    measures = pd.read_json(r.text)
    measures['created'] = pd.to_datetime(measures['created'])
    #print(measures.head(5))
    #days = pd.DataFrame()
    days = measures.resample('D', on='created').agg(['min','max', 'sum'])
    #print(days.label)

    for d in days.iterrows():
        getMeasureValues(hostId, d[0])

    #measures.to_csv("host_" + str(hostId) + "_measure.csv", index = None, header = True)


def getMeasureValues(hostId, dt):
    print("Get measures values from host " + str(hostId) + " in " + dt.strftime("%Y-%m-%d"), flush=True)
    r = requests.get("http://143.54.2.90/api/host/" + str(hostId) + "/measure/" + dt.strftime("%Y/%m/%d") )
    f = open("host_" + str(hostId) + "_measure_value_" + dt.strftime("%Y-%m-%d") + ".json", "w")
    f.write(r.text)
    f.close()




if __name__ == "__main__":
    main()
